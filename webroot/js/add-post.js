$(function() {
  $(`#add-post`).on('submit', async (e) => {
    // Prevent form submission
    e.preventDefault();
    
    // Initialize a new GraphQL instance
    const gql = new GraphQL('/graphql');

    // Add our mutation
    gql.setQuery(`mutation($title: String!, $thumbnail: String, $published: Int!, $body: String!, $tags: [PostTag]) {
      addBlogPost(title: $title, thumbnail: $thumbnail, published: $published, body: $body, tags: $tags) {
        success
        message
        id
      }
    }`);

    // Add our basic fields
    const fields = $(`#add-post`).serializeArray();
    gql.addVariable('title', fields.find((field) => field.name == 'title').value);
    gql.addVariable('body', fields.find((field) => field.name == 'body').value);
    gql.addVariable('published', parseInt(fields.find((field) => field.name == 'published').value));
    gql.addVariable('thumbnail', fields.find((field) => field.name == 'thumbnail').value);

    // Add our tags
    let tags = [];
    for(let tag of fields.filter((field) => field.name == 'tags[]')) {
      tags.push({'id': parseInt(tag.value)});
    }
    gql.addVariable('tags', tags);

    // Send our mutation
    const res = await gql.execute();
    
    // Show the result
    if(res.data.addBlogPost.success) {
      toastr.success(`${res.data.addBlogPost.message} You'll be redirected shortly!`, 'Blog Post added!');
      window.location.replace(`/admin/blog/edit/${res.data.addBlogPost.id}`);
    } else {
      toastr.error(res.data.addBlogPost.message, 'Could not add blogpost');
    }
  });
});