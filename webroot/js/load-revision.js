$(function() {
  $(`button.btn[data-action='load-revision']`).on('click', async (e) => {
    // Get our revision
    // If no result was found, show a toast and return
    let revision = $(e)[0].currentTarget.dataset['revision'];
    if(!revision) {
      toastr.error('Something went wrong while opening the revision', 'Whoops!');
      return;
    }

    // Initialize a new GraphQL query
    const gql = new GraphQL('/graphql');

    // Add our query
    gql.setQuery(`query($hash: String!) {
      postRevision(hash: $hash) {
        success
        message
        data {
          article
          body
          created
          hash
          published
          thumbnail
        }
      }
    }`);

    // Add the revision hash
    gql.addVariable('hash', revision);

    // Execute our query
    const res = await gql.execute();

    // Check if the request was a success
    // If not, show a toast
    if(!res.data.postRevision.success)  {
      toastr.error(res.data.postRevision.message, 'Whoops!');
      return;
    }
    if(!res.data.postRevision.data) {
      toastr.error('Something went wrong while trying to load this revision!', 'Whoops!');
      return;
    }

    // Set all content
    $(`#edit-post input[name='thumbnail']`).val(res.data.postRevision.data.thumbnail);
    tinymce.activeEditor.setContent(res.data.postRevision.data.body);
    toastr.success('Revision has been loaded!', 'Hoozay!');
  });
});
