$(function() {
  let tagTemplate;

  $(`button[data-action='create-tag']`).on(`click`, async (e) => {
    // Prevent form submission
    e.preventDefault();

    // Check if the tag template was already compiled
    // If not, compile it now
    if(!tagTemplate) tagTemplate = Handlebars.compile($(`#tag-template`).html());

    // Store the title
    const fields = $(`#edit-post`).serializeArray();
    const title = fields.find((field) => field.name == 'new-tag-name').value;

    // Check if a title was set
    if(!title) {
      toastr.error('Tag name may not be empty!', 'Could not create tag!');
      return;
    }

    // Initialize a new GraphQL instance
    const gql = new GraphQL('/graphql');

    // Add our mutation
    gql.setQuery(`mutation($title: String!) {
      createTag(title: $title) {
        success
        message
        id
      }
    }`);

    // Add our variables
    gql.addVariable('title', title);

    // Send our mutation
    const res = await gql.execute();

    // Check whether the query executed successfully
    if(res.data.createTag.success) {
      toastr.success(res.data.createTag.message, 'Tag created');
      $(`#tags-list`).append(tagTemplate({title: title, id: res.data.createTag.id}));
      $(`[type='checkbox']#checkbox-${res.data.createTag.id}`).bootstrapToggle();
    } else {
      toastr.error(res.data.createTag.message, 'Could not create tag!');
    }
  });
});