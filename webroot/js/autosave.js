$(function() {
  let timer;
  let interval = 2000;

  // Listen for keyup events
  $('#edit-post').keyup(setTimer);
  tinymce.activeEditor.on('keyup', setTimer);

  // Load our autosaved form when TinyMCE has initialized
  tinymce.activeEditor.on('init', load);

  function setTimer() {
    // Clear out last timer
    clearTimeout(timer);

    // Set a new timer
    timer = setTimeout(store, interval);
  }

  async function store() {
    // Get our form value
    let form = $(`#edit-post`).serializeArray();

    // Get the id of this post
    let id = form.find((field) => field.name == 'post-id').value;

    // Replace the body with the TinyMCE content
    form.find((field) => field.name == 'body').value = tinymce.activeEditor.getContent();

    // Store data in our localstorage as JSON
    await localStorage.setItem(`blog-autosave-${id}`, JSON.stringify(form));

    // Show a message that autosave went successfully
    toastr.success(`Your post has been autosaved!`, `Autosave success!`);
  }

  async function load() {
    // Get the id of this post
    let form = $(`#edit-post`).serializeArray();
    let id = form.find((field) => field.name == 'post-id').value;

    // Load our data from local storage
    // If no data was found, return
    let data = await localStorage.getItem(`blog-autosave-${id}`);
    if(!data) return;

    // Parse our JSON
    data = JSON.parse(data);

    // Check whether the post has been modified
    // If so, delete the local version and do not attempt to load it any further
    let lastModifiedForm = form.find((field) => field.name == 'post-last-modified').value;
    let lastModifiedLocal = data.find((field) => field.name == 'post-last-modified').value;
    if(lastModifiedForm > lastModifiedLocal) {
      localStorage.removeItem(`blog-autosave-${id}`);
      return;
    }

    // Update our form
    $(`#edit-post input[name='title']`).val(data.find((field) => field.name == 'title').value);
    $(`#edit-post input[name='thumbnail']`).val(data.find((field) => field.name == 'thumbnail').value);
    tinymce.activeEditor.setContent(data.find((field) => field.name == 'body').value);

    // Show a message that loading autosave went successfully
    toastr.success(`We've loaded up your last autosaved version`, `Loaded autosaved version!`);
  }
});
