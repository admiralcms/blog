$(function() {
  $(`#edit-post`).on('submit', async (e) => {
    // Prevent form submission
    e.preventDefault();
    
    // Initialize a new GraphQL instance
    const gql = new GraphQL('/graphql');

    // Add our mutation
    gql.setQuery(`mutation($id: Int!, $title: String!, $thumbnail: String, $published: Int!, $body: String!, $tags: [PostTag]) {
      editBlogPost(id: $id, title: $title, thumbnail: $thumbnail, published: $published, body: $body, tags: $tags) {
        success
        message
      }
    }`);

    // Add our basic fields
    const fields = $(`#edit-post`).serializeArray();
    gql.addVariable('id', parseInt(fields.find((field) => field.name == 'post-id').value));
    gql.addVariable('title', fields.find((field) => field.name == 'title').value);
    gql.addVariable('body', tinymce.activeEditor.getContent());
    gql.addVariable('published', parseInt(fields.find((field) => field.name == 'published').value));
    gql.addVariable('thumbnail', fields.find((field) => field.name == 'thumbnail').value);

    // Add our tags
    let tags = [];
    for(let tag of fields.filter((field) => field.name == 'tags[]')) {
      tags.push({'id': parseInt(tag.value)});
    }
    gql.addVariable('tags', tags);

    // Send our mutation
    const res = await gql.execute();
    
    // Show the result
    if(res.data.editBlogPost.success) {
      toastr.success(res.data.editBlogPost.message, 'Blog Post Updated!');
    } else {
      toastr.error(res.data.editBlogPost.message, 'Could not update blogpost');
    }
  });

  $(`#edit-post button.btn[data-action='preview']`).on('click', async (e) => {
    // Prevent form submission
    e.preventDefault();

    // Initialize a new GraphQL instance
    const gql = new GraphQL('/graphql');

    // Add our mutation
    gql.setQuery(`mutation($id: Int!, $title: String!, $published: Int!, $body: String!, $tags: [PostTag]) {
      addBlogPostRevision(id: $id, title: $title, published: $published, body: $body, tags: $tags) {
        success
        message
        revision
      }
    }`);

    // Add our basic fields
    const fields = $(`#edit-post`).serializeArray();
    gql.addVariable('id', parseInt(fields.find((field) => field.name == 'post-id').value));
    gql.addVariable('title', fields.find((field) => field.name == 'title').value);
    gql.addVariable('body', tinymce.activeEditor.getContent());
    gql.addVariable('published', parseInt(fields.find((field) => field.name == 'published').value));

    // Add our tags
    let tags = [];
    for(let tag of fields.filter((field) => field.name == 'tags[]')) {
      tags.push({'id': parseInt(tag.value)});
    }
    gql.addVariable('tags', tags);

    // Send our mutation
    const res = await gql.execute();

    // Show the result
    if(res.data.addBlogPostRevision.success) {
      toastr.success(`${res.data.addBlogPostRevision.message} It'll be opened in a new tab!`, 'Revision saved!');
      window.open(res.data.addBlogPostRevision.revision);
    } else {
      toastr.error(res.data.addBlogPostRevision.message, 'Could not save revision');
    }
  });
});