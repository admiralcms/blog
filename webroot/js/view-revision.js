$(function() {
  $(`button.btn[data-action='view-revision']`).on('click', async (e) => {
    // Get our revision
    // If no result was found, show a toast and return
    let revision = $(e)[0].currentTarget.dataset['revision'];
    if(!revision) {
      toastr.error('Something went wrong while opening the revision', 'Whoops!');
      return;
    }

    // Build our url
    // Then open a new tab
    let url = previewUrlTemplate.replaceAll(`:slug`, articleSlug).replaceAll(`:hash`, revision);
    window.open(url);
  });
});
