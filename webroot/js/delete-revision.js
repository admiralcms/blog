$(function() {
  $(`button.btn[data-action='delete-revision']`).on('click', async (e) => {
    // Get our revision
    // If no result was found, show a toast and return
    let revision = $(e)[0].currentTarget.dataset['revision'];
    if(!revision) {
      toastr.error('Something went wrong while opening the revision', 'Whoops!');
      return;
    }

    // Initialize a new GraphQL query
    const gql = new GraphQL('/graphql');

    // Add our query
    gql.setQuery(`mutation($hash: String!) {
      deletePostRevision(hash: $hash) {
        success
        message
      }
    }`);

    // Add the revision hash
    gql.addVariable('hash', revision);

    // Execute our query
    const res = await gql.execute();

    if(res.data.deletePostRevision.success) {
      toastr.success(res.data.deletePostRevision.message, 'Hoozay!');
      $(`#revision-${revision}`).remove();
    } else {
      toastr.error(res.data.deletePostRevision.message, 'Whoops!');
    }
  });
});
