# Permissions List
Below is a comprehensive list of permissions used in this package.

| Node | Description |
|------|-------------|
| admiral.admiral.cms.access | Allow the user to access the CMS |
| admiral.blog.posts.edit | Allow the user to add and edit blogposts |
| admiral.blog.tags.create | Allow the user to create new tags |
| admiral.blog.posts.revisions.delete | Allow the user to delete revisions |
