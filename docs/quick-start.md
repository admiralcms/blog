# Quick Start
Here is a quickstart guide for implementing Admiral in your project.

## Dependencies
- CakePHP `3.x` (CakePHP 4.x currently unsupported)
- PHP `7.4.x` (PHP `8.x` currently unsupported)
- Admiral `2.x`

## Installing
Installing the blog plugin takes a few steps to get to work properly.
In this section, we'll cover those steps so you can get started as quickly as possible!

### Installing Package
```
composer require admiral/blog
```

### Loading into CakePHP
In order to get this plugin to work, we'll need to load it into CakePHP by adding the following to our `src/Application.php`:
```php
// src/Application.php
public function bootstrap() {
  parent::bootstrap();

  $this->addPlugin('Admiral/Blog',['bootstrap' => true]);
}
```

### Running Migrations
Next, you'll need to run the migrations.
```
bin/cake migrations migrate -p Admiral/Blog
```

### Getting Roles and Permissions setup
In order to grant users access to certain features (like creating and editting blog posts), we need to add some permissions.  
This can be done by modifying your `config/permissions.yaml`.  
A list of permissions for this plugin can be found [here](permissions-list.md).

### Enabling Previews
In order to get previews to work, you need to setup a minor config in your `config/app.php`:
```php
return [
  'Admiral' => [
    'blog' => [
      'previewUrl' => '/path/to/blog/:slug/:hash'
    ]
  ]
];
```

Use placeholders `:slug` and `:revision` according to the route you have setup:
```php
Router::scope('/', function (RouteBuilder $routes) {
  $routes->connect('/blog/article/:slug/:hash',['plugin'=> null,'controller' => 'Blog', 'action' => 'view'])
      ->setPass(['slug', 'hash']);
}
```