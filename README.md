# Blog
Blog plugin for use with Admiral.
**warning:** This repository is undergoing major development and should not be used in production websites yet.

## Installing
For installing this plugin, please refer to the [Quickstart](docs/quick-start.md).

## Version Compatibility
Below a table of version compatibility.  
Please note that when a new version is released, support for older versions by the maintainer drop *immediately*.
| Plugin Version | Admiral Version | CakePHP Version |
|----------------|-----------------|-----------------|
| 1.x            | 1.x-2.x         | 3.x             |