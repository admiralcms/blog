<?php
  namespace Admiral\Blog\Controller;

  use Admiral\Admiral\Controller\AppController as BaseController;
  use Cake\Core\Configure;

  class AppController extends BaseController {
    public function initialize(): void {
      parent::initialize();
      $this->Auth->allow(['view', 'display']);

      $this->loadModel('Admiral/Admiral.Options');

      $this->set('menuItems',Configure::read('Admiral\Admiral.menuItems'));
    }
  }