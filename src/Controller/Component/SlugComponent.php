<?php
namespace Admiral\Blog\Controller\Component;

use Cake\Controller\Component;
use Cake\Utility\Inflector;
use Cake\ORM\TableRegistry;

class SlugComponent extends Component {
  /**
   * Generate a unique slug based on the title and check it against the database
   * When the slug is not unique, keep increasing the number until a unique one has been found.
   * 
   * @param string $title the title on which to base the slug
   * @return string the resulting unique slug
   */
  public function generateSlug($title){
    // Get out articles table from the registry
    $articles = TableRegistry::getTableLocator()->get('Articles');
    
    // Check if the slug exists, and if not, add a number behind it
    $slug = Inflector::slug($title);
    if($articles->exists(['slug' => $slug])){
      $iteration = 1;
      while($articles->exists(['slug' => $slug])){
        $slug= Inflector::slug($title . "-" . $iteration);
        $iteration++;
      }
    }

    return $slug;
  }
}