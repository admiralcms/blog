<?php
  namespace Admiral\Blog\Controller;

  use Admiral\Admiral\Permission;
  use Admiral\Admiral\User;

  use Cake\Core\Configure;
  use Cake\Routing\Router;
  use Cake\Filesystem\File;
  use Cake\Event\Event;

  class BlogController extends AppController {
    public function beforeFilter(Event $event) {
      $this->Auth->autoRedirect = false;
    }

    public function initialize(): void {
      parent::initialize();

      // Load the required models
      $this->loadModel('Admiral/Blog.Articles');
      $this->loadModel('Admiral/Admiral.Users');
      $this->loadModel('Admiral/Blog.ArticlesTags');
      $this->loadModel('Admiral/Blog.ArticlesRevisions');

      $this->viewBuilder()->setClassName('Admiral/Admiral.App');

      // Check whether the current action is allowed without auth
      if(!in_array($this->request->getParam('action'),$this->Auth->allowedActions)){
        // current action requires auth
        // Check whether the user is logged in or not
        if(User::get()){
          // Check whether the user is allowed to access the CMS

          if(!Permission::check('admiral.admiral.cms.access', 1)){
            // User does not have the right permission
            $this->redirect(['controller' => 'Users', 'action' => 'my_account', 'my-account']); # redirect to the general my-account page
          }
        }else{
          // User is not logged in
          // Check whether we are on the login page
          if($this->request->getParam('action') != 'login'){
            // We are not on the login page
            $redirUrl = Router::url(["controller" => $this->request->params['controller'], "action" => $this->request->params['action']]); # Build the current url for redirection
            $this->redirect(['plugin'=>null,'controller' => 'Admin', 'action' => 'login', 'redir' => $redirUrl]); # Redirect to the login page
          }
        }
      }
    }

    public function index(){
      $this->set('title', 'All Posts');

      $this->viewBuilder()->setLayout('Admiral/Admiral.admin'); # Change the layout to the admin_login layout

      // Get the blogposts from the database
      $articles = $this->Articles->find('all',['order' => ['Articles.created' => 'DESC']])->contain(['Tags','Users']);
      $this->set('articles',$articles);
    }

    public function edit($id = null) {
      // Check whether the user has the rights to be here
      // Redirect to the dashboard if not
      if(!Permission::check('admiral.blog.posts.edit', 1)){
        $this->Flash->error('You do not have the permissions to do this');
        return $this->redirect(['plugin'=>null, 'controller'=>'admin','action'=>'index']);
      }

      // Get a list of tags
      $tags = $this->Articles->Tags->find('list');

      // Get the article data
      $article = $this->Articles->findById($id)->contain([
        'Tags',
        'ArticlesRevisions' => [
          'sort' => ['ArticlesRevisions.created' => 'DESC'],
          'Authors',
        ],
      ])
      ->firstOrFail();

      // Get the article body
      $file = new File(ROOT . DS . 'blog-posts' . DS . $article->hash . '.txt');

      // Set our view variables
      $this->set('article', $article);
      $this->set('body', $file->read());
      $this->set('title', 'Editing Post: ' . $article->title);
      $this->set('tags', $tags);
      $this->set('previewUrlTemplate', Router::url('/', true) . Configure::read('Admiral.blog.previewUrl'));

      // Change the layout
      $this->viewBuilder()->setLayout('Admiral/Admiral.admin');
    }

    public function add(){
      // Check whether the user has the rights to be here
      // If not, redirect to the dashboard
      if(!Permission::check('admiral.blog.posts.edit', 1)){
        $this->Flash->error('You do not have the permissions to do this');
        return $this->redirect(['plugin'=>null, 'controller'=>'admin','action'=>'index']);
      }


      // Get the list of tags
      $tags = $this->Articles->Tags->find('list');

      // Set our view values
      $this->set('tags', $tags);
      $this->set('title', 'Add Post');

      // Change the layout
      $this->viewBuilder()->setLayout('Admiral/Admiral.admin');
    }

    public function display() {
      $user = $this->Users->findById($this->Auth->user('id'))->contain(['Roles' => ['Permissions']])->first();
      $this->set('user', $user);
    }
  }
