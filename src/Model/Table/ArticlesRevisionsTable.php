<?php
namespace Admiral\Blog\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ArticlesRevisions Model
 *
 * @property \Admiral\Blog\Model\Table\ArticlesTable&\Cake\ORM\Association\BelongsTo $Articles
 *
 * @method \Admiral\Blog\Model\Entity\ArticlesRevision get($primaryKey, $options = [])
 * @method \Admiral\Blog\Model\Entity\ArticlesRevision newEntity($data = null, array $options = [])
 * @method \Admiral\Blog\Model\Entity\ArticlesRevision[] newEntities(array $data, array $options = [])
 * @method \Admiral\Blog\Model\Entity\ArticlesRevision|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Admiral\Blog\Model\Entity\ArticlesRevision saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Admiral\Blog\Model\Entity\ArticlesRevision patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Admiral\Blog\Model\Entity\ArticlesRevision[] patchEntities($entities, array $data, array $options = [])
 * @method \Admiral\Blog\Model\Entity\ArticlesRevision findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ArticlesRevisionsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('articles_revisions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Articles', [
            'foreignKey' => 'article_id',
            'joinType' => 'INNER',
            'className' => 'Admiral/Blog.Articles',
        ]);

        $this->belongsTo('Authors', [
          'foreignKey' => 'author_id',
          'joinType' => 'LEFT',
          'className' => 'Admiral/Admiral.Users',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('revision')
            ->maxLength('revision', 255)
            ->requirePresence('revision', 'create')
            ->notEmptyString('revision');

        $validator
            ->integer('published')
            ->notEmptyString('published');

        $validator
            ->scalar('thumbnail')
            ->maxLength('thumbnail', 255)
            ->allowEmptyString('thumbnail');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['article_id'], 'Articles'));

        return $rules;
    }
}
