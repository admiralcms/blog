<?php
namespace Admiral\Blog\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArticlesRevision Entity
 *
 * @property int $id
 * @property int $article_id
 * @property string $revision
 * @property int $published
 * @property \Cake\I18n\FrozenTime $created
 * @property string|null $thumbnail
 *
 * @property \Admiral\Blog\Model\Entity\Article $article
 */
class ArticlesRevision extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'article_id' => true,
        'revision' => true,
        'published' => true,
        'created' => true,
        'thumbnail' => true,
        'article' => true,
        'author_id' => true,
    ];
}
