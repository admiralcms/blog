<?php
namespace Admiral\Blog\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArticlesTag Entity
 *
 * @property int $tag_id
 * @property int $article_id
 *
 * @property Admiral\Blog\Model\Entity\Tag $tag
 * @property Admiral\Blog\Model\Entity\Article $article
 */
class ArticlesTag extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tag_id' => true,
        'article_id' => true
    ];
}
