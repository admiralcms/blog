<?php
namespace Admiral\Blog\View\Helper;

use Cake\View\Helper;

class TagsHelper extends Helper {
  public function hasTag($article, string $tag) {
    return collection($article->tags)->firstMatch(['id' => $tag]) !== null;
  }
}
