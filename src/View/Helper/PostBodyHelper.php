<?php
namespace Admiral\Blog\View\Helper;

use Cake\View\Helper;
use Cake\Filesystem\File;
use Admiral\Blog\Model\Entity\Article;

class PostBodyHelper extends Helper{
  /**
   * Get the body of a blog post
   * 
   * @param \Admiral\Blog\Model\Entity\Article $article
   * @return string|null The body of the post
   */
  public function get(Article $article): ?string {
    // Get a file handle
    $file = new File(ROOT . DS . 'blog-posts' . DS . $article->hash . '.txt');

    // Check if the file exists
    // If not, return null
    if(!$file->exists()) return null;

    // Read the body
    $body = $file->read();

    // Return the body
    return $body;
  }

  /**
   * Get an excerpt for the post
   * 
   * @param \Admiral\Blog\Model\Entity\Article $article
   * @return string The excerpt
   */
  public function getExcerpt($article, int $wordLimit = 50) {
    // Check if the ShortcodeHelper was loaded
    // If so, strip shortcode tags
    if(!empty($this->getView()->Shortcode)) $article = $this->getView()->Shortcode->stripShortcodes($article);

    // Strip HTML tags
    $article = strip_tags($article);

    // Limit the words to the limit
    if (str_word_count($article, 0) > $wordLimit) {
      $words = str_word_count($article, 2);
      $pos = array_keys($words);
      $article = substr($article, 0, $pos[$wordLimit]) . '...';
    }

    return $article;
  }

}