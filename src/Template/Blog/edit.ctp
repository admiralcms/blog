<div class="row">
  <div class="col-lg-12">
    <?= $this->Flash->render(); ?>
  </div>
</div>

<form id="edit-post">
  <div class="row">
    <div class="col-lg-9">
      <div class="form-group">
        <?= $this->Form->control("title",array("placeholder"=>"Post Title","label"=>false,"class"=>"form-control rounded-0","value"=>$article->title)); ?>
        <small>
          slug:
          <span class="text-muted"><?= h($article->slug); ?>
        </small>
      </div>
      <div class="form-group">
        <?= $this->Ui->texteditor->create($body, [
          'class' => 'form-control',
          'name' => 'body'
        ]); ?>
      </div>
    </div>
    <div class="col-lg-3">
      <div class="row mb-2">
        <div class="col-lg-12">
          <?= $this->Ui->card->start(); ?>
            <?= $this->Ui->card->header('Publish'); ?>
            <?= $this->Ui->card->bodystart(); ?>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="published" value="1" <?php if($article->published == 1){ ?>checked<?php } ?>>
                <label class="form-check-label" for="published">
                  Public
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="published" value="0" <?php if($article->published == 0){ ?>checked<?php } ?>>
                <label class="form-check-label" for="published">
                  Private
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="published" value="2" <?php if($article->published == 2){ ?>checked<?php } ?>>
                <label class="form-check-label" for="published">
                  Unlisted
                </label>
              </div>
              <?= $this->Form->button('Save',["type"=>"submit","class"=>"btn btn-primary rounded-0"]); ?>
              <?= $this->Form->button('Preview',["class"=>"btn btn-secondary rounded-0", "data-action" => "preview"]); ?>
            <?= $this->Ui->card->bodyend(); ?>
          <?= $this->Ui->card->end(); ?>
        </div>
      </div>
      <div class="row mb-2">
        <div class="col-lg-12">
          <?= $this->Ui->card->start(); ?>
            <?= $this->Ui->card->header('Tags'); ?>
            <?= $this->Ui->card->bodystart(); ?>
              <div class="form-group" id="tags-list">
                <?php foreach($tags as $id => $title): ?>
                  <div class="form-check">
                    <?= $this->Ui->checkbox->checkbox($this->Tags->hasTag($article,$id), [
                      'attrs' => [
                        'name' => 'tags[]',
                        'value' => h($id),
                      ]
                    ]); ?>
                    <label class="form-check-label">
                      <?= h($title); ?>
                    </label>
                  </div>
                <?php endforeach; ?>
              </div>
              <div class="form-group mt-3">
                <div class="input-group mb-3">
                  <input type="text" class="form-control rounded-0" placeholder="Create tag" name="new-tag-name">
                  <button type="button" class="btn btn-success rounded-0" data-action="create-tag">
                    <i class="fas fa-plus"></i>
                  </button>
                </div>
              </div>
            <?= $this->Ui->card->bodyend(); ?>
          <?= $this->Ui->card->end(); ?>
        </div>
      </div>
      <div class="row mb-2">
        <div class="col-lg-12">
          <?= $this->Ui->card->start(); ?>
            <?= $this->Ui->card->header('Thumbnail'); ?>
            <?= $this->Ui->card->bodystart(); ?>
            <div class="form-group">
              <?= $this->Form->control("thumbnail",array("placeholder"=>"Post Thumbnail URL","label"=>false,"class"=>"form-control rounded-0","value"=>$article->thumbnail)); ?>
              <small>
                Please enter the full URL to a thumbnail (Leave empty for no thumbnail).
              </small>
            </div>
            <?= $this->Ui->card->bodyend(); ?>
          <?= $this->Ui->card->end(); ?>
        </div>
      </div>
    </div>
  </div>
  <input type="hidden" name="post-id" value="<?= h($article->id); ?>">
  <input type="hidden" name="post-last-modified" value="<?= h($article->modified); ?>">
</form>

<div class="row">
  <div class="col-lg-7">
    <?= $this->Ui->table->start(); ?>
      <?= $this->Ui->table->header(['Date', 'Status', 'Author', 'Actions']); ?>
      <?= $this->Ui->table->bodystart(); ?>
        <?php foreach($article->articles_revisions as $revision): ?>
          <?= $this->Ui->table->rowstart('revision-' . $revision->revision); ?>
            <?= $this->Ui->table->columnstart(); ?>
              <?= $revision->created; ?>
            <?= $this->Ui->table->columnend(); ?>
            <?= $this->Ui->table->columnstart(); ?>
              <?php
                switch($revision->published){
                  case -1:
                    echo 'Unlisted';
                    break;
                  case 0:
                    echo 'Private';
                    break;
                  case 1:
                    echo 'Public';
                    break;
                }
              ?>
            <?= $this->Ui->table->columnend(); ?>
            <?= $this->Ui->table->columnstart(); ?>
              <?php if(!empty($revision->author)): ?>
                <img src="<?= h($revision->author->avatar()); ?>" style="max-height: 32px; max-width: 32px;"> <?= h($revision->author->username); ?>
              <?php else: ?>
                Unknown User
              <?php endif; ?>
            <?= $this->Ui->table->columnend(); ?>
            <?= $this->Ui->table->columnstart(); ?>
              <button class="btn btn-success rounded-0" data-action="load-revision" data-revision="<?= h($revision->revision); ?>">
                <i class="fas fa-edit"></i>
              </button>
              <button class="btn btn-primary rounded-0" data-action="view-revision" data-revision="<?= h($revision->revision); ?>">
                <i class="fas fa-eye"></i>
              </button>
              <button class="btn btn-danger rounded-0" data-action="delete-revision" data-revision="<?= h($revision->revision); ?>">
                <i class="fas fa-trash-alt"></i>
              </button>
            <?= $this->Ui->table->columnend(); ?>
          <?= $this->Ui->table->rowend(); ?>
        <?php endforeach; ?>
      <?= $this->Ui->table->bodyend(); ?>
    <?= $this->Ui->table->end(); ?>
  </div>
</div>


<?php \Admiral\Admiral\Asset::script('Admiral/Blog.edit-post'); ?>
<?php \Admiral\Admiral\Asset::script('Admiral/Blog.create-tag'); ?>
<?php \Admiral\Admiral\Asset::script('Admiral/Blog.autosave'); ?>
<?php \Admiral\Admiral\Asset::script('Admiral/Blog.view-revision'); ?>
<?php \Admiral\Admiral\Asset::script('Admiral/Blog.load-revision'); ?>
<?php \Admiral\Admiral\Asset::script('Admiral/Blog.delete-revision'); ?>

<script>
  const previewUrlTemplate = `<?= h($previewUrlTemplate); ?>`;
  const articleSlug = `<?= h($article->slug); ?>`;
</script>

<script id="tag-template" type="text/x-handlebars-template">
  <div class="form-check">
    <?= $this->Ui->checkbox->checkbox(true, [
      'attrs' => [
        'name' => 'tags[]',
        'value' => '{{ id }}',
        'id' => 'checkbox-{{ id }}',
      ]
    ]); ?>
    <label class="form-check-label">
      {{ title }}
    </label>
  </div>
</script>
