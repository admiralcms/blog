<div class="row">
  <div class="col-lg-12">
    <?= $this->Flash->render(); ?>
  </div>
</div>

<form id="add-post">
  <div class="row">
    <div class="col-lg-9">
      <div class="form-group">
        <?= $this->Form->control("title",["placeholder"=>"Post Title","label"=>false,"class"=>"form-control rounded-0"]); ?>
      </div>
      <div class="form-group">
        <?= $this->Ui->texteditor->create('', [
          'class' => 'form-control',
          'name' => 'body'
        ]); ?>
      </div>
    </div>
    <div class="col-lg-3">
      <div class="row mb-2">
        <div class="col-lg-12">
          <?= $this->Ui->card->start(); ?>
            <?= $this->Ui->card->header('Publish'); ?>
            <?= $this->Ui->card->bodystart(); ?>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="published" value="1">
                <label class="form-check-label" for="published">
                  Public
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="published" value="0" checked>
                <label class="form-check-label" for="published">
                  Private
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="published" value="2">
                <label class="form-check-label" for="published">
                  Unlisted
                </label>
              </div>
              <?= $this->Form->button('Save',["type"=>"submit","class"=>"btn btn-primary rounded-0"]); ?>
            <?= $this->Ui->card->bodyend(); ?>
          <?= $this->Ui->card->end(); ?>
        </div>
      </div>
      <div class="row mb-2">
        <div class="col-lg-12">
          <?= $this->Ui->card->start(); ?>
            <?= $this->Ui->card->header('Tags'); ?>
            <?= $this->Ui->card->bodystart(); ?>
              <?php foreach($tags as $id => $title): ?>
                <div class="form-check">
                  <?= $this->Ui->checkbox->checkbox(false, [
                    'attrs' => [
                      'name' => 'tags[]',
                      'value' => h($id),
                    ]
                  ]); ?>
                  <label class="form-check-label">
                    <?= htmlentities($title); ?>
                  </label>
                </div>
              <?php endforeach; ?>
            <?= $this->Ui->card->bodyend(); ?>
          <?= $this->Ui->card->end(); ?>
        </div>
      </div>
      <div class="row mb-2">
        <div class="col-lg-12">
          <?= $this->Ui->card->start(); ?>
            <?= $this->Ui->card->header('Thumbnail'); ?>
            <?= $this->Ui->card->bodystart(); ?>
            <div class="form-group">
              <?= $this->Form->control("thumbnail",array("placeholder"=>"Post Thumbnail URL","label"=>false,"class"=>"form-control rounded-0")); ?>
              <small>
                Please enter the full URL to a thumbnail (Leave empty for no thumbnail).
              </small>
            </div>
            <?= $this->Ui->card->bodyend(); ?>
          <?= $this->Ui->card->end(); ?>
        </div>
      </div>
    </div>
  </div>
</form>

<?php \Admiral\Admiral\Asset::script('Admiral/Blog.add-post'); ?>
