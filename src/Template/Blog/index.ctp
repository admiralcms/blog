<?php
  use Cake\I18n\Time;
?>
<?= $this->Ui->table->start(['checkall' => true]); ?>
  <?= $this->Ui->table->header([
    'Title',
    'Author' => ['width' => 2],
    'Tags' => ['width' => 2],
    'Date' => ['width' => 2],
    'View',
    'Edit',
    'Delete'
  ]); ?>
  <tbody>
    <?php foreach($articles as $article): ?>
      <?= $this->Ui->table->rowstart(); ?>
        <td><?= h($article->title); ?></td>
        <td><?= h($article->user->username);?></td>
        <td>
          <?php foreach($article->tags as $id => $tag): ?>
            <span class="badge badge-secondary rounded-0"><?= h($tag->title); ?></span>
          <?php endforeach; ?>
        </td>
        <td>
          <?php if($article->modified > $article->created): ?>
            Modified <?= h($article->modified->format("Y-m-d")); ?> at <?= h($article->modified->format("h:i:s")); ?>
          <?php else: ?>
            Created <?= h($article->created->format("Y-m-d")); ?> at <?= h($article->created->format("h:i:s")); ?>
          <?php endif; ?>
        </td>
        <td>
          <p data-placement="top" data-toggle="tooltip" title="View">
            <button class="btn btn-primary btn-xs" data-title="View" data-toggle="modal" data-target="#view" >
              <span class="far fa-eye"></span>
            </button>
          </p>
        </td>
        <td>
          <p data-placement="top" data-toggle="tooltip" title="Edit">
            <?=
              $this->Html->link(
                $this->Html->tag(
                  'button',
                  $this->Html->tag('span',null,['class'=>'far fa-edit','escape'=>false]),
                  ['escape'=>false,'class'=>'btn btn-primary btn-xs']
                ),
                ['plugin'=>'Admiral/Blog','controller'=>'blog','action'=>'edit',$article->id],
                ['escape'=>false]
              );
            ?>
          </p>
        </td>
        <td>
          <p data-placement="top" data-toggle="tooltip" title="Delete">
            <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" >
              <span class="far fa-trash-alt"></span>
            </button>
          </p>
        </td>
      <?= $this->Ui->table->rowend(); ?>
    <?php endforeach; ?>
  </tbody>
  <!-- This is actually the footer -->
  <?= $this->Ui->table->header([
    'Title',
    'Author' => ['width' => 2],
    'Tags' => ['width' => 2],
    'Date' => ['width' => 2],
    'View',
    'Edit',
    'Delete'
  ]); ?>
<?= $this->Ui->table->end(); ?>

<script>
  $(function(){
    // Initialize all the tooltips
    $("[data-toggle=tooltip]").tooltip();
  });
</script>
