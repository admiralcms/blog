<?php
  namespace Admiral\Blog\GraphQL\Type;

  use Admiral\GraphQL\Types;

  class RevisionUpdateStatusType {
    public function config() {
      return [
        'name' => 'RevisionUpdateStatus',
        'fields' => function() {
          return [
            'success' => [
              'type' => Types::get('boolean'),
              'description' => 'Whether this request was a success',
            ],
            'message' => [
              'type' => Types::get('string'),
              'description' => 'Optional message of this request'
            ],
            'revision' => [
              'type' => Types::get('string'),
              'description' => 'URL to read the revision'
            ]
          ];
        }
      ];
    }
  }