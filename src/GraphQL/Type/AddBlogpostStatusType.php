<?php
  namespace Admiral\Blog\GraphQL\Type;

  use Admiral\GraphQL\Types;

  class AddBlogpostStatusType {
    public function config() {
      return [
        'name' => 'AddBlogpostStatus',
        'fields' => function() {
          return [
            'success' => [
              'type' => Types::get('boolean'),
              'description' => 'Whether this request was a success',
            ],
            'message' => [
              'type' => Types::get('string'),
              'description' => 'Optional message of this request'
            ],
            'id' => [
              'type' => Types::get('int'),
              'description' => 'id of the blogpost'
            ]
          ];
        }
      ];
    }
  }