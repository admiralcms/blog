<?php
  namespace Admiral\Blog\GraphQL\Type;

  use Admiral\GraphQL\Types;

  class PostTagType {
    public function config() {
      return [
        'name' => 'PostTag',
        'fields' => function() {
          return [
            'id' => [
              'type' => Types::get('int'),
            ],
            'title' => [
              'type' => Types::get('string'),
            ]
          ];
        }
      ];
    }
  }