<?php
  namespace Admiral\Blog\GraphQL\Type;

  use Admiral\GraphQL\Types;
  use GraphQL\Type\Definition\ObjectType;

  class PostRevisionType {
    public function config() {
      return [
        'name' => 'PostRevision',
        'fields' => function() {
          return [
            'success' => [
              'type' => Types::get('boolean'),
            ],
            'message' => [
              'type' => Types::get('string'),
            ],
            'data' => new ObjectType([
              'name' => 'PostRevisionData',
              'fields' => [
                'article' => [
                  'type' => Types::get('int'),
                ],
                'hash' => [
                  'type' => Types::get('string'),
                ],
                'published' => [
                  'type' => Types::get('int'),
                ],
                'created' => [
                  'type' => Types::get('string'),
                ],
                'thumbnail' => [
                  'type' => Types::get('string'),
                ],
                'body' => [
                  'type' => Types::get('string'),
                ],
              ],
            ]),
          ];
        }
      ];
    }
  }
