<?php
  namespace Admiral\Blog\GraphQL\Resolver;

  use Admiral\Blog\GraphQL\Datasource\BlogPostDatasource;

  class BlogPostResolver {
    private $datasource;

    public function __construct() {
      $this->datasource = new BlogPostDatasource();
    }

    public function addBlogPost($rootValue, $args, $context, $info) {
      return $this->datasource->addBlogPost($args);
    }

    public function editBlogPost($rootValue, $args, $context, $info) {
      return $this->datasource->editBlogPost($args);
    }

    public function addBlogPostRevision($rootValue, $args, $context, $info) {
      return $this->datasource->addRevision($args);
    }

    public function getRevision($rootValue, $args, $context, $info) {
      return $this->datasource->getRevision($args);
    }

    public function deleteRevision($rootValue, $args, $context, $info) {
      return $this->datasource->deleteRevision($args);
    }
  }
