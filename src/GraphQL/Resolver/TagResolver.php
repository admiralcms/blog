<?php
  namespace Admiral\Blog\GraphQL\Resolver;

  use Admiral\Blog\GraphQL\Datasource\TagDatasource;

  class TagResolver {
    private $datasource;

    public function __construct() {
      $this->datasource = new TagDatasource();
    }

    public function createTag($rootValue, $args, $context, $info) {
      return $this->datasource->createTag($args);
    }
  }