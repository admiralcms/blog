<?php
  namespace Admiral\Blog\GraphQL\Datasource;

  use Admiral\Admiral\Permission;
  use Admiral\Admiral\User;

  use Cake\ORM\TableRegistry;
  use Cake\I18n\Time;
  use Cake\Core\Configure;
  use Cake\Utility\Security;
  use Cake\Utility\Text;
  use Cake\Filesystem\File;
  use Cake\Routing\Router;

  class BlogPostDatasource {
    private $Articles;
    private $ArticlesTags;

    public function __construct() {
      $this->Articles = TableRegistry::getTableLocator()->get('Admiral/Blog.Articles');
      $this->ArticlesTags = TableRegistry::getTableLocator()->get('Admiral/Blog.ArticlesTags');
      $this->ArticlesRevisions = TableRegistry::getTableLocator()->get('Admiral/Blog.ArticlesRevisions');
    }

    public function editBlogPost(array $args = []) {
      // Check whether the user:
      // - Is logged in
      // - Has the required permission
      if(!Permission::check('admiral.blog.posts.edit', 1)) {
        return [
          'success' => false,
          'message' => 'You do not have the permission to do this!',
        ];
      }

      // Get the existing article data
      $article = $this->Articles->findById($args['id'])->first();
      if(!$article) {
        return [
          'success' => false,
          'message' => 'A post with this ID was not found!',
        ];
      }

      // Get the current time
      $now = new Time();

      // Create a unique name for the file
      $hash = Security::hash(
        $args['title'] . $article->slug . $args['body'] . $now->format('Y-m-d H:i:s') . $now->format('Y-m-d H:i:s'),
        'sha256',
        false
      );

      // Write the body to file
      $file = new File(ROOT . DS . 'blog-posts' . DS . $hash . '.txt', true);
      $file->write($args['body']);

      // Update the entity
      $article = $this->Articles->patchEntity($article,[
        'title' => $args['title'],
        'published' => $args['published'],
        'modified' => $now,
        'hash' => $hash,
        'thumbnail' => !empty($args['thumbnail']) ? $args['thumbnail'] : null,
      ]);

      // Save our post
      if(!$this->Articles->save($article)) {
        return [
          'success' => false,
          'message' => 'There was an issue adding this post. Please try again later!',
        ];
      }

      // Create a new revision
      $revision = $this->ArticlesRevisions->newEntity([
        'article_id' => $article->id,
        'revision' => $hash,
        'published' => $args['published'],
        'created' => $now,
        'thumbnail' => !empty($args['thumbnail']) ? $args['thumbnail'] : null,
        'author_id' => User::get()['id'],
      ]);

      // Save our revision
      if(!$this->ArticlesRevisions->save($revision)) {
        return [
          'success' => false,
          'message' => 'There was an issue adding this post. Please try again later!',
        ];
      }

      // Delete all current tags so we can easily replace them with the new tags
      $this->ArticlesTags->deleteAll(['article_id' => $args['id']]);

      // Create array for the tags
      $tags = [];

      // Check if there are any tags selected
      if(empty($args['tags'])){
        // No tags selected, add a default
        $tags[] = ['tag_id' => 1, 'article_id' => $article->id];
      } else {
        // Tags selected, add them to the list
        foreach($args['tags'] as $obj => $tag){
          $tags[] = ['tag_id' => $tag['id'], 'article_id' => $article->id];
        }
      }

      // Save the tags
      // Return an error if this failed
      if(!$this->ArticlesTags->saveMany($this->ArticlesTags->newEntities($tags))){
        return [
          'success' => false,
          'message' => 'The post has been updated successfully, but there was an issue saving tags for it.',
        ];
      }

      // Everything alright
      return [
        'success' => true,
        'message' => 'The post has been updated successfully!',
      ];
    }

    public function addBlogPost(array $args = []) {
      // Check whether the user:
      // - Is logged in
      // - Has the required permission
      if(!Permission::check('admiral.blog.posts.edit', 1)) {
        return [
          'success' => false,
          'message' => 'You do not have the permission to do this!',
        ];
      }

      // Get the current time
      $now = new Time();

      // Generate a slug
      $slug = Text::slug($args['title']);

      // Check if our slug is taken
      // If it does, append a number
      // Increase number until unique slug is found
      if($this->Articles->exists(['slug' => $slug])){
        $iteration = 1;
        while($this->Articles->exists(['slug' => $slug])) {
          $slug = Text::slug($args['title'] . "-" . $iteration);
          $iteration++;
        }
      }

      // Turn the slug into lowercase
      $slug = strtolower($slug);

      // Create a unique name for the file
      $hash = Security::hash(
        $args['title'] . $slug . $args['body'] . $now->format('Y-m-d H:i:s') . $now->format('Y-m-d H:i:s'),
        'sha256',
        false
      );

      // Write the body to file
      $file = new File(ROOT . DS . 'blog-posts' . DS . $hash . '.txt');
      $file->write($args['body']);

      // Create an entity for the article
      $article = $this->Articles->newEntity([
        'user_id' => User::get()['id'],
        'title' => $args['title'],
        'slug' => $slug,
        'hash' => $hash,
        'published' => $args['published'],
        'created' => $now,
        'modified' => $now,
        'thumbnail' => !empty($args['thumbnail']) ? $args['thumbnail'] : null,
      ]);

      // Add the article
      // Return an error if this failed
      if(($article = $this->Articles->save($article)) == false){
        return [
          'success' => false,
          'message' => 'There was an issue adding this post. Please try again later!'
        ];
      }

      // Create a new revision
      $revision = $this->ArticlesRevisions->newEntity([
        'article_id' => $article->id,
        'revision' => $hash,
        'published' => $args['published'],
        'created' => $now,
        'thumbnail' => !empty($args['thumbnail']) ? $args['thumbnail'] : null,
        'author_id' => User::get()['id'],
      ]);

      // Save our revision
      if(!$this->ArticlesRevisions->save($revision)) {
        return [
          'success' => false,
          'message' => 'There was an issue adding this post. Please try again later!',
        ];
      }

      // Create array for the tags
      $tags = [];

      // Check if there are any tags selected
      if(empty($args['tags'])){
        // No tags selected, add a default
        $tags[] = ['tag_id' => 1, 'article_id' => $article->id];
      } else {
        // Tags selected, add them to the list
        foreach($args['tags'] as $obj => $tag){
          $tags[] = ['tag_id' => $tag['id'], 'article_id' => $article->id];
        }
      }

      // Save the tags
      // Return an error if this failed
      if(!$this->ArticlesTags->saveMany($this->ArticlesTags->newEntities($tags))){
        return [
          'success' => false,
          'message' => 'The post has been added successfully, but there was an issue saving tags for it.',
          'id' => $article->id,
        ];
      }

      // Everything alright
      return [
        'success' => true,
        'message' => 'The post has been added successfully!',
        'id' => $article->id,
      ];
    }

    public function addRevision(array $args = []) {
      // Check whether the user:
      // - Is logged in
      // - Has the required permission
      if(!Permission::check('admiral.blog.posts.edit', 1)) {
        return [
          'success' => false,
          'message' => 'You do not have the permission to do this!',
        ];
      }

      // Get the existing article data
      $article = $this->Articles->findById($args['id'])->first();
      if(!$article) {
        return [
          'success' => false,
          'message' => 'A post with this ID was not found!',
        ];
      }

      // Get the current time
      $now = new Time();

      // Create a unique name for the file
      $hash = Security::hash(
        $args['title'] . $article->slug . $args['body'] . $now->format('Y-m-d H:i:s') . $now->format('Y-m-d H:i:s'),
        'sha256',
        false
      );

      // Write the body to file
      $file = new File(ROOT . DS . 'blog-posts' . DS . $hash . '.txt', true);
      $file->write($args['body']);

      // Create a new revision
      $revision = $this->ArticlesRevisions->newEntity([
        'article_id' => $article->id,
        'revision' => $hash,
        'published' => $args['published'],
        'created' => $now,
        'thumbnail' => !empty($args['thumbnail']) ? $args['thumbnail'] : null,
      ]);

      // Save our revision
      if(!$this->ArticlesRevisions->save($revision)) {
        return [
          'success' => false,
          'message' => 'There was an issue adding this post. Please try again later!',
        ];
      }

      // Delete all current tags so we can easily replace them with the new tags
      $this->ArticlesTags->deleteAll(['article_id' => $args['id']]);

      // Create array for the tags
      $tags = [];

      // Check if there are any tags selected
      if(empty($args['tags'])){
        // No tags selected, add a default
        $tags[] = ['tag_id' => 1, 'article_id' => $article->id];
      } else {
        // Tags selected, add them to the list
        foreach($args['tags'] as $obj => $tag){
          $tags[] = ['tag_id' => $tag['id'], 'article_id' => $article->id];
        }
      }

      // Save the tags
      // Return an error if this failed
      if(!$this->ArticlesTags->saveMany($this->ArticlesTags->newEntities($tags))){
        return [
          'success' => false,
          'message' => 'The post has been updated successfully, but there was an issue saving tags for it.',
        ];
      }

      // Build the preview url
      $preview = Router::url('/', true);
      $preview .= Configure::read('Admiral.blog.previewUrl');
      $preview = str_replace(':slug', $article->slug, $preview);
      $preview = str_replace(':hash', $hash, $preview);

      // Everything alright
      return [
        'success' => true,
        'message' => 'The post has been updated successfully!',
        'revision' => $preview,
      ];
    }

    public function getRevision(array $args = []) {
      // Check if a hash was specified
      // If not return
      if(empty($args['hash'])) return ['success' => false, 'message' => 'Revision hash cannot be left empty'];

      // Make sure the user is authorized to do delete revisions
      if(!Permission::check('admiral.blog.posts.edit', 1)) {
        return [
          'success' => false,
          'message' => 'You do not have the permission to do this!',
        ];
      }

      // Get the revision from the database
      // If no result was found, return
      $revision = $this->ArticlesRevisions->findByRevision($args['hash'])->first();
      if(!$revision) return ['success' => false, 'message' => 'Requested revision could not be found'];

      // Open a filehandle for the rile
      // If no file was found, return
      $file = new File(ROOT . DS . 'blog-posts' . DS . $args['hash'] . '.txt', false);
      if(!$revision) return ['success' => false, 'message' => 'Revision body could not be found'];

      // Return our data
      return [
        'success' => true,
        'message' => null,
        'data' => [
          'article' => $revision->article_id,
          'hash' => $revision->revision,
          'published' => $revision->published,
          'created' => $revision->created,
          'thumbnail' => $revision->thumbnail,
          'body' => $file->read(),
        ],
      ];
    }

    public function deleteRevision(array $args = []) {
      // Check if a hash was specified
      // If not return
      if(empty($args['hash'])) return ['success' => false, 'message' => 'Revision hash cannot be left empty'];

      // Make sure the user is authorized to do delete revisions
      if(!Permission::check('admiral.blog.posts.revisions.delete', 1)) {
        return [
          'success' => false,
          'message' => 'You do not have the permission to do this!',
        ];
      }

      // Get the revision from the database
      // If no result was found, return
      $revision = $this->ArticlesRevisions->findByRevision($args['hash'])->contain(['Articles'])->first();
      if(!$revision) return ['success' => false, 'message' => 'Requested revision could not be found'];

      // Check if this revision is the latest
      // If so, return
      if($revision->article->hash == $revision->revision) return ['success' => false, 'message' => 'Cannot remove revision as it\'s the latest!'];

      // Open a filehandle for the file
      $file = new File(ROOT . DS . 'blog-posts' . DS . $args['hash'] . '.txt', false);

      // Remove the revision from the database
      // If failed, return
      if(!$this->ArticlesRevisions->delete($revision)) return ['success' => false, 'message' => 'Could not delete revision from database'];

      // Remove the file
      // If failed, return
      if(!$file->delete()) return ['success' => false, 'message' => 'Could not delete revision from filesystem'];

      // Everything went right
      return ['success' => true, 'message' => 'Revision has been deleted!'];
    }
  }
