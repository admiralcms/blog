<?php
  namespace Admiral\Blog\GraphQL\Datasource; 
  
  use Admiral\Admiral\Permission;
  use Admiral\Admiral\User;
  
  use Cake\ORM\TableRegistry;
  use Cake\I18n\Time;
  use Cake\Core\Configure;
  use Cake\Utility\Security;
  use Cake\Utility\Text;
  use Cake\Filesystem\File;
  use Cake\Routing\Router;

  class TagDatasource {
    private $Tags;

    public function __construct() {
      $this->Tags = TableRegistry::getTableLocator()->get('Admiral/Blog.Tags');
    }

    public function createTag(array $args = []) {
      // Check if the user has the required permission
      // If not, return an error
      if(!Permission::check('admiral.blog.tags.create', 1)) return [
        'success' => false,
        'message' => 'You do not have the permission required to do this!',
      ];

      // Check if a tag with this name already exists
      // If not, return an error
      if($this->Tags->exists(['title' => $args['title']])) return [
        'success' => false,
        'message' => 'A tag with this name already exists!',
      ];

      // Create a new entity
      $entity = $this->Tags->newEntity([
        'title' => $args['title'],
      ]);

      // Save our tag
      if(!$this->Tags->save($entity))  return [
        'success' => false,
        'message' => 'Something went wrong while trying to create this tag!',
      ];

      return [
        'success' => true,
        'message' => 'Tag has been created!',
        'id' => $entity->id,
      ];
    }
  }