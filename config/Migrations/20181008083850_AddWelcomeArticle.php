<?php
use Migrations\AbstractMigration;
use Cake\ORM\TableRegistry;

class AddWelcomeArticle extends AbstractMigration {
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change() {
    // Load the required tables from the registry
    $articlesTable = TableRegistry::getTableLocator()->get('Articles');
    $usersTable = TableRegistry::getTableLocator()->get('Admiral/Admiral.Users');
    $tagsTable = TableRegistry::getTableLocator()->get('Tags');
    $articlesTagsTable = TableRegistry::getTableLocator()->get('ArticlesTags');

    // Create a new article
    $article = $articlesTable->newEntity();
    $article->user_id = null;
    $article->title = "Hello World!";
    $article->slug = "hello-world";
    $article->body = "This is the first article on your new website!";
    $article->published = true;
    $article->created = date("Y-m-d H:i:s");
    $article->modified = date("Y-m-d H:i:s");
    $articlesTable->save($article);

    // Get the id for the Uncategorized tag
    $tagId = ($tagsTable->findByTitle('Uncategorized')->first())->id;

    // Add a tag to the article
    $articleTag = $articlesTagsTable->newEntity();
    $articleTag->article_id = $article->id;
    $articleTag->tag_id = $tagId;
    $articlesTagsTable->save($articleTag);
  }
}
