<?php
use Migrations\AbstractMigration;

class RenameBodyToHash extends AbstractMigration {
  public function up() {
    $this->table('articles')
      ->renameColumn('body', 'hash')
      ->save();
  }
}
