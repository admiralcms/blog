<?php
use Migrations\AbstractMigration;
use Cake\ORM\TableRegistry;

class AddInitialTags extends AbstractMigration {
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change() {
    // Load the TagsTable from the registry
    $tagsTable = TableRegistry::getTableLocator()->get('Tags');

    // Create a new tag
    $tag = $tagsTable->newEntity();
    $tag->title = 'Uncategorized';
    $tag->created = date("Y-m-d H:i:s");
    $tag->modified = date("Y-m-d H:i:s");

    // Save the tag
    $tagsTable->save($tag);
  }
}
