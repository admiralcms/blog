<?php
use Migrations\AbstractMigration;

class AddInitialTables extends AbstractMigration {
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change() {
    // Create the articles table
    $table = $this->table('articles')
                  ->addColumn('user_id','integer',['default' => null,'limit' => 11,'null' => true])
                  ->addColumn('title','string',['default' => null,'null' => false])
                  ->addColumn('slug','string',['default' => null,'null' => false])
                  ->addIndex('slug',['unique'=>true])
                  ->addColumn('body','text',['default' => null,'null' => false])
                  ->addColumn('published','boolean',['default' => null,'null' => false])
                  ->addColumn('created','datetime',['default' => null,'null' => false])
                  ->addColumn('modified','datetime',['default' => null,'null' => false])
                  ->addForeignKey('user_id','users','id', ['delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'])
                  ->save();

    // Create the tags table
    $table = $this->table('tags')
                  ->addColumn('title','string',['default' => null,'null' => false])
                  ->addColumn('created','datetime',['default' => null,'null' => false])
                  ->addColumn('modified','datetime',['default' => null,'null' => false])
                  ->save();

    // Create the articles_tags table
    $table = $this->table('articles_tags',['id' => false,'primary_key' => ['article_id','tag_id']])
                  ->addColumn('article_id','integer',['default' => null,'limit' => 11,'null' => false])
                  ->addColumn('tag_id','integer',['default' => null,'limit' => 11,'null' => false])
                  ->addForeignKey('article_id','articles','id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
                  ->addForeignKey('tag_id','tags','id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
                  ->save();
  }
}
