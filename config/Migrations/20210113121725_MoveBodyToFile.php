<?php
use Migrations\AbstractMigration;

use Cake\Filesystem\File;
use Cake\Filesystem\Folder;
use Cake\Utility\Security;

class MoveBodyToFile extends AbstractMigration {
  public function up() {
    // Get all post bodies
    $stmt = $this->query('SELECT * from articles');
    $posts = $stmt->fetchAll();
    foreach($posts as $post) {
      $this->store($post);
    }
  }

  private function store(array $post) {
    // Create the blog folder at the root if it doesn't exist
    $folder = new Folder(ROOT . DS . 'blog-posts', true);

    // Create a unique name for the file
    $hash = Security::hash(
      $post['title'] . $post['slug'] . $post['body'] . $post['created'] . $post['modified'],
      'sha256',
      false
    );

    // Open a file handle
    $file = new File(ROOT . DS . 'blog-posts' . DS . $hash . '.txt');

    // Write the body to the file
    $file->write($post['body']);

    // Update our table
    $this->execute("UPDATE `articles` SET `body`='" . $hash . "' WHERE `id`='".$post['id']."'");
  }
}
