<?php
use Migrations\AbstractMigration;

class AddThumbnailField extends AbstractMigration {
  public function change(): void {
    // Add the thumbnail field to the article's main data
    $this->table('articles')
      ->addColumn('thumbnail','string', ['default' => null,'null' => true])
      ->save();

    // Add the thumbnail field to the revisions data
    $this->table('articles_revisions')
      ->addColumn('thumbnail','string', ['default' => null,'null' => true])
      ->save();
  }
}
