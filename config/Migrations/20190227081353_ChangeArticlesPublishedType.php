<?php
use Migrations\AbstractMigration;

class ChangeArticlesPublishedType extends AbstractMigration {
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change() {
    $this->table('articles')
         ->changeColumn('published', 'integer', ['limit' => 1])
         ->save();
  }
}
