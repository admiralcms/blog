<?php
use Migrations\AbstractMigration;

class AddRevisionsTable extends AbstractMigration {
  public function up() {
    $this->table('articles_revisions')
      ->addColumn('article_id','integer', ['default' => null, 'null' => false])
      ->addColumn('revision','string', ['default' => null,'null' => false])
      ->addColumn('published','integer', ['default' => 0])
      ->addColumn('created','datetime', ['default' => null,'null' => false])
      ->addForeignKey('article_id','articles','id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
      ->save();
  }
}
