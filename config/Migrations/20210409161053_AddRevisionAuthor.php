<?php
use Migrations\AbstractMigration;

class AddRevisionAuthor extends AbstractMigration {
  public function up() {
    $this->table('articles_revisions')
      ->addColumn('author_id','integer', ['default' => null,'null' => true])
      ->addForeignKey('author_id','users','id', ['delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'])
      ->save();
  }
}
