<?php
  use Cake\Routing\Route\DashedRoute;
  use Cake\Routing\Router;

  /**
   * Routes for the admin dashboard
   */
  Router::plugin('Admiral/Blog',['path' => '/admin'],function ($routes) {
    $routes->get('/blog', ['controller' => 'Blog', 'action' => 'index']);
    $routes->get('/blog/add', ['controller' => 'Blog', 'action' => 'add']);
    $routes->connect('/blog/edit/:id',['controller' => 'Blog', 'action' => 'edit'])
      ->setPass(['id']);

    $routes->fallbacks(DashedRoute::class);
  });