<?php
  use Admiral\Admiral\Menu;
  use Admiral\GraphQL\Types;
  use Admiral\GraphQL\Types\Definition\QueryType;
  use Admiral\GraphQL\Types\Definition\MutationType;

  if(PHP_SAPI === 'fpm-fcgi'){
    Menu::add('main_menu', [
      'name' => 'blog',
      'label' => 'Blog',
      'icon' => 'fas fa-book',
      'weight' => 0,
      'children' => [
        [
          'label' => __('All Posts'),
          'url' => [
            'plugin'=>'Admiral/Blog',
            'controller' => 'Blog',
            'action'=>'index'
          ]
        ],
        [
          'label' => __('Add Post'),
          'url' => [
            'plugin'=>'Admiral/Blog',
            'controller' => 'Blog',
            'action'=>'add'
          ]
        ]
      ]
    ]);
  }

  Types::register('PostTag', (new \Admiral\Blog\GraphQL\Type\PostTagType())->config());
  Types::registerInput('PostTag', (new \Admiral\Blog\GraphQL\Type\PostTagType())->config());
  Types::register('RevisionUpdateStatus', (new \Admiral\Blog\GraphQL\Type\RevisionUpdateStatusType())->config());
  Types::register('TagCreateStatus', (new \Admiral\Blog\GraphQL\Type\TagCreateStatusType())->config());
  Types::register('AddBlogpostStatus', (new \Admiral\Blog\GraphQL\Type\AddBlogpostStatusType())->config());
  Types::register('PostRevision', (new \Admiral\Blog\GraphQL\Type\PostRevisionType())->config());

  QueryType::addField('postRevision', [
    'type' => Types::get('PostRevision'),
    'description' => 'A specific revision for a post',
    'args' => [
      'hash' => Types::input('string')
    ],
    'resolve' => function($rootValue, $args, $context, $info) {
      return (new \Admiral\Blog\GraphQL\Resolver\BlogPostResolver)->getRevision($rootValue, $args, $context, $info);
    }
  ]);

  MutationType::addField('addBlogPost',[
    'type' => Types::get('AddBlogpostStatus'),
    'description' => 'Add a blogpost',
    'args' => [
      'title' => Types::input('string'),
      'published' => Types::input('int'),
      'body' => Types::input('string'),
      'tags' => Types::listOf(Types::input('PostTag')),
      'thumbnail' => Types::input('string'),
    ],
    'resolve' => function($rootValue, $args, $context, $info) {
      return (new \Admiral\Blog\GraphQL\Resolver\BlogPostResolver)->addBlogPost($rootValue, $args, $context, $info);
    }
  ]);

  MutationType::addField('editBlogPost',[
    'type' => Types::get('Status'),
    'description' => 'Edit a blogpost',
    'args' => [
      'id' => Types::input('int'),
      'title' => Types::input('string'),
      'published' => Types::input('int'),
      'body' => Types::input('string'),
      'tags' => Types::listOf(Types::input('PostTag')),
      'thumbnail' => Types::input('string'),
    ],
    'resolve' => function($rootValue, $args, $context, $info) {
      return (new \Admiral\Blog\GraphQL\Resolver\BlogPostResolver)->editBlogPost($rootValue, $args, $context, $info);
    }
  ]);

  MutationType::addField('addBlogPostRevision',[
    'type' => Types::get('RevisionUpdateStatus'),
    'description' => 'Add a revision to a blogpost',
    'args' => [
      'id' => Types::input('int'),
      'title' => Types::input('string'),
      'published' => Types::input('int'),
      'body' => Types::input('string'),
      'tags' => Types::listOf(Types::input('PostTag')),
      'thumbnail' => Types::input('string'),
    ],
    'resolve' => function($rootValue, $args, $context, $info) {
      return (new \Admiral\Blog\GraphQL\Resolver\BlogPostResolver)->addBlogPostRevision($rootValue, $args, $context, $info);
    }
  ]);

  MutationType::addField('deletePostRevision', [
    'type' => Types::get('Status'),
    'description' => 'Remove a revision from a blogpost',
    'args' => [
      'hash' => Types::input('string')
    ],
    'resolve' => function($rootValue, $args, $context, $info) {
      return (new \Admiral\Blog\GraphQL\Resolver\BlogPostResolver)->deleteRevision($rootValue, $args, $context, $info);
    }
  ]);

  MutationType::addField('createTag',[
    'type' => Types::get('TagCreateStatus'),
    'description' => 'Create a new tag',
    'args' => [
      'title' => Types::input('string'),
    ],
    'resolve' => function($rootValue, $args, $context, $info) {
      return (new \Admiral\Blog\GraphQL\Resolver\TagResolver)->createTag($rootValue, $args, $context, $info);
    }
  ]);
