<?php
namespace Admiral\Blog\Test\TestCase\Model\Table;

use Admiral\Blog\Model\Table\ArticlesRevisionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Admiral\Blog\Model\Table\ArticlesRevisionsTable Test Case
 */
class ArticlesRevisionsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Admiral\Blog\Model\Table\ArticlesRevisionsTable
     */
    public $ArticlesRevisions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Admiral/Blog.ArticlesRevisions',
        'plugin.Admiral/Blog.Articles',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ArticlesRevisions') ? [] : ['className' => ArticlesRevisionsTable::class];
        $this->ArticlesRevisions = TableRegistry::getTableLocator()->get('ArticlesRevisions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ArticlesRevisions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
